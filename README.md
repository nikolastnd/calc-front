# Setup application
Install dependencies
>$ yarn

Run tests
>$ yarn test

Start application
>$ yarn start
