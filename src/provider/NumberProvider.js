import React, { useState } from 'react';
import {calculate} from '../services/MathService'
import {OperationEnum, MathFunctionEnum} from '../enums/OperationEnum'
export const NumberContext = React.createContext();

const NumberProvider = props => {
  const [number, setNumber] = useState('');
  const [storedNumber, setStoredNumber] = useState('');
  const [functionType, setFunctionType] = useState('');

  const handleSetDisplayValue = num => {
    if ((!number.includes('.') || num !== '.') && number.length < 8) {
      setNumber(`${(number + num).replace(/^0+/, '')}`);
    }
  };

  const handleSetStoredValue = () => {
    setStoredNumber(number);
    setNumber('');
  };

  const handleClearValue = () => {
    setNumber('');
    setStoredNumber('');
    setFunctionType('');
  };

  const handleSetCalcFunction = type => {
    type = type.replace('/', '\u00F7').replace('*', 'x')
    if (number) {
      setFunctionType(type);
      handleSetStoredValue();
    }
    if (storedNumber) {
      setFunctionType(type);
    }
  };

  const handleKeyPress = event => {
    event.preventDefault()
    event.stopPropagation()
    const numbers = '123456789.'
    const operations = '+-/*'
    if (numbers.includes(event.key)) {
      handleSetDisplayValue(event.key)
    }else if(operations.includes(event.key)){
      handleSetCalcFunction(event.key)
    }else if(event.key=='Enter'){
      doMath()
    }else if(event.key === "Delete"){
      handleClearValue()
    }
  };

  const doMath = async () => {
    
    if (number && storedNumber && functionType) {
      var response = {}
      response = await calculate(MathFunctionEnum[functionType], storedNumber, number)
      if (response && response.data){
        setStoredNumber(response.data.result);
      }
      setNumber('');
    }
  };

  return (
    <NumberContext.Provider
      value={{
        doMath,
        handleKeyPress,
        functionType,
        handleClearValue,
        handleSetCalcFunction,
        handleSetDisplayValue,
        handleSetStoredValue,
        number,
        storedNumber,
        setNumber,
      }}
    >
      {props.children}
    </NumberContext.Provider>
  );
};

export default NumberProvider;
