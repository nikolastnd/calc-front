import styled from "styled-components";

const primaryColor = "#3A4655";
const secondaryColor = "#425062";
const primaryTextColor = "#fff";
const OperationButtonColor = "#384352";

export const CalculatorStyles = styled.div`
  background-color: ${primaryColor};
  width: 100%;
  min-height: 100vh;
  display: grid;
  justify-items: center;
  grid-template-columns: 1fr;
  .display {
    h1 {
      font-size: 4rem;
      color: ${primaryTextColor};
      text-align: center;
    }
    @media (orientation: landscape) {
      height: 10vh;
    }
  }
  .keys {
    display: grid;
    grid-gap: 10px;
    grid-template-columns: repeat(4, 1fr);
    padding: 0px 0px 30px;
    width: 450px;
    margin: 0 auto;
    button {
      width: 100%;
      border: 4px solid ${secondaryColor};
      font-size: 2rem;
      color: ${primaryTextColor};
      background: ${secondaryColor};
      outline: none;
      transition: 270ms all;
      cursor: pointer;
      :hover {
        color: ${secondaryColor};
        background: ${primaryTextColor};
      }
    }
    button.equal-button {
      color: #2d3436;
      background-color: ${OperationButtonColor};
      color: ${primaryTextColor};
      grid-row-end: span 4;
      height: 100%;
      padding: 0px;
      margin: 0px;
      :hover {
        background-color: ${primaryTextColor};
        color: ${OperationButtonColor};
      }
    }
    button.function-button {
      background-color: ${OperationButtonColor};
      :hover {
        background-color: ${primaryTextColor};
        color: ${OperationButtonColor};
      }
    }
    @media (max-width: 500px) {
      width: 90vw;
    }
  }
`;

export const DisplayStyles = styled.div`
  max-width: 450px;
  width: 90%;
  margin: 5vh 5vw;
  border: 4px solid ${secondaryColor};
  background: ${secondaryColor};
  h2,
  p {
    text-align: center;
    color: ${primaryTextColor};
  }
  h2 {
    font-size: 2.5rem;
    margin: 0;
    text-align: right;
  }
  p {
    margin-bottom: 30%;
    padding: 5%;
    font-size: 1.3rem;
    border-bottom: 1px solid white;
  }
`;
