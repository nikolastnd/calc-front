import React from 'react';
import PropTypes from "prop-types";

const Button = ({ value, onClick, className }) => {
  return (
    <button type="button" onClick={()=>{onClick(value)}} className={className}>
      {value}
    </button>
  );
};

Button.defaultProps = {
  onClick: ()=>{},
  value: '',
  className: '',
}

Button.propTypes = {
  onClick: PropTypes.func,
  value: PropTypes.string,
  className: PropTypes.string,
};



export default Button;
