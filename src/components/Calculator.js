import React, { useContext } from 'react';
import Button from './Button';
import Display from './Display';
import EqualButton from './EqualButton';
import { CalculatorStyles } from './styles/Styles';
import { NumberContext } from '../provider/NumberProvider';


const Calculator = () => {
 
  const { handleKeyPress, handleSetDisplayValue, handleSetCalcFunction, handleClearValue } = useContext(NumberContext);
  return (
    <CalculatorStyles onKeyPress={handleKeyPress}>
      <Display />
      <div className="keys">
        <Button value={"+"} onClick={handleSetCalcFunction} className="function-button" />
        <Button value={"-"} onClick={handleSetCalcFunction} className="function-button" />
        <Button value={"x"} onClick={handleSetCalcFunction} className="function-button" />
        <Button value={"\u00F7"} onClick={handleSetCalcFunction} className="function-button" />
        <Button value={"7"} onClick={handleSetDisplayValue} />
        <Button value={"8"} onClick={handleSetDisplayValue} />
        <Button value={"9"} onClick={handleSetDisplayValue} />
        <EqualButton />
        <Button value={"4"} onClick={handleSetDisplayValue}/>
        <Button value={"5"} onClick={handleSetDisplayValue} />
        <Button value={"6"} onClick={handleSetDisplayValue} />
        <Button value={"1"} onClick={handleSetDisplayValue} />
        <Button value={"2"} onClick={handleSetDisplayValue} />
        <Button value={"3"} onClick={handleSetDisplayValue} />
        <Button value={"0"} onClick={handleSetDisplayValue} />
        <Button value="." onClick={handleSetDisplayValue} />
        <Button value={"C"} onClick={handleClearValue} className="function-button" />
      </div>
    </CalculatorStyles>
  );
}

export default Calculator;
