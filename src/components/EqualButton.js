import React, { useContext } from 'react';
import { NumberContext } from '../provider/NumberProvider';

const EqualButton = () => {
  const { doMath } = useContext(NumberContext);
  const calculate = (e)=>{
    e.preventDefault();
    doMath()
  }
  return (
    <button className="equal-button" type="button" onClick={calculate}>
      =
    </button>
  );
};

export default EqualButton;
