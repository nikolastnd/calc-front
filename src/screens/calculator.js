import React from 'react';
import Calculator from '../components/Calculator';
import NumberProvider from '../provider/NumberProvider';


const CalculatorScreen = () => (
    <NumberProvider>
        <Calculator />
    </NumberProvider>
  );
    
export default CalculatorScreen;