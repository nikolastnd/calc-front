
const OperationEnum = {
    SUM: 'add',
    SUBTRACT: 'subtract',
    DIVIDE: 'divide',
    MULTIPLY: 'multiply',
}

const MathFunctionEnum = {
    '+': OperationEnum.SUM,
    '-': OperationEnum.SUBTRACT,
    'x': OperationEnum.MULTIPLY,
    '\u00F7': OperationEnum.DIVIDE,
}

export  { OperationEnum, MathFunctionEnum };