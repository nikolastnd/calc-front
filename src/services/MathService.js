import axios from 'axios'

export const mathService = () => {
    return axios.create({
        baseURL: process.env.REACT_APP_CALC_API_URL,
        headers: {"Content-Type": "application/json"}
    })
}

const calculate = (operation, first_value, second_value) => {
    return mathService().post(operation, {first_value, second_value})
}

export {calculate};
export default mathService;